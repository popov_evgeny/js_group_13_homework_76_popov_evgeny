export class Message {
  constructor(
    public author: string,
    public message: string,
    public id: string,
    public datetime: string
  ) {}
}

export interface InterfaceMessage {
  author: string,
  message: string
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../../models/message.model';
import { MessagesService } from '../../services/messages.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit, OnDestroy {
  arrMessages: Message[] = [];
  arrMessagesSubscription!: Subscription;
  loadingSubscription!: Subscription;
  loading = false;
  interval!: number;

  constructor(private messagesService: MessagesService) { }

  ngOnInit(): void {
    this.loadingSubscription = this.messagesService.getLoading.subscribe( (loadingData: boolean) => {
      this.loading = loadingData;
    });
    this.arrMessagesSubscription = this.messagesService.changeArrayMessages.subscribe( messages => {
      this.arrMessages = messages.reverse();
    });
    this.interval = this.messagesService.getAllMessages();
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
    this.arrMessagesSubscription.unsubscribe();
    clearInterval(this.interval);
  }
}

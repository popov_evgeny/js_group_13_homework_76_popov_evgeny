import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MessagesService } from '../../services/messages.service';
import { NgForm } from '@angular/forms';
import { InterfaceMessage } from '../../models/message.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages-form',
  templateUrl: './messages-form.component.html',
  styleUrls: ['./messages-form.component.sass']
})
export class MessagesFormComponent implements OnInit, OnDestroy{
  @ViewChild('form') form!: NgForm;
  loadingSubscription!: Subscription;
  loading = false;

  constructor(
    private messageService: MessagesService,
    ) {}

  ngOnInit() {
    this.loadingSubscription = this.messageService.postLoading.subscribe( loading => {
      this.loading = loading;
    });
  }

  onSubmit() {
    const value: InterfaceMessage = this.form.value;
    this.messageService.creatMessage(value);
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }
}

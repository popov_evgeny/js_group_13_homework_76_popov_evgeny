import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InterfaceMessage, Message } from '../models/message.model';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  private arrayMessages: Message[] = [];
  changeArrayMessages = new Subject<Message[]>();
  getLoading = new Subject<boolean>();
  postLoading = new Subject<boolean>();
  date = '';

  constructor(
    private http: HttpClient,
    private router: Router
  ) {}

  getAllMessages() {
    this.getLoading.next(true);
    return setInterval(() => {
      this.http.get<Message[]>(environment.apiUrl + `/messages?datetime=${this.date}`).pipe(map(response => {
        return response.map(messageData => {
          return new Message(
            messageData.author,
            messageData.message,
            messageData.id,
            messageData.datetime
          )
        })
      })).subscribe((messages: Message[]) => {
        if (this.arrayMessages.length === 0) {
          this.arrayMessages = messages;
          if (this.arrayMessages.length > 0) {
            this.date = this.arrayMessages[this.arrayMessages.length - 1].datetime;
          }
          this.changeArrayMessages.next(this.arrayMessages.slice());
        } else {
          this.arrayMessages = this.arrayMessages.concat(messages);
          this.date = this.arrayMessages[this.arrayMessages.length - 1].datetime;
          this.changeArrayMessages.next(this.arrayMessages.slice(this.arrayMessages.length - 30, this.arrayMessages.length));
        }
        this.getLoading.next(false);
      });
    }, 2000);
  }

  creatMessage(message: InterfaceMessage) {
    this.postLoading.next(true);
    this.http.post<Message>(environment.apiUrl + '/messages', message).subscribe(() => {
      this.postLoading.next(false);
      void this.router.navigate(['/']);
    });
  }
}

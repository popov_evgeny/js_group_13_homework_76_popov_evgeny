import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-found',
  template: `<h1>Not found!</h1>`,
  styles: [`
    h1
      color: white
      font-size: 80px
      font-style: italic
      text-align: center
      padding: 50px

  `]
})
export class NotFoundComponent implements OnInit {

  constructor(private router: Router) {}

  ngOnInit() {
    setTimeout(() => {
      void this.router.navigate(['/']);
    },2500);
  }
}

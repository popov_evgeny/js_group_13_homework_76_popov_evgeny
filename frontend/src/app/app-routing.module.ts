import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MessagesComponent } from './pages/messages/messages.component';
import { MessagesFormComponent } from './pages/messages-form/messages-form.component';
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
  {path: '', component: MessagesComponent},
  {path: 'creat-message', component: MessagesFormComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

const express = require('express');
const db = require('../dataBase');

const router = express.Router();

router.get('/', (req, res) => {
  let messages = [];
  const date = new Date(req.query.datetime);
  if (!req.query.datetime){
    if (db.getItems().length < 30) {
      messages = db.getItems();
    } else {
      messages = db.getItems().slice(db.getItems().length - 30, db.getItems().length);
    }
    return res.send(messages);
  } else {
    if (isNaN(date.getDate())){
      return res.status(400).send({message: 'Incorrect date-time!'});
    } else {
      let index = db.getItems().findIndex(message => message.datetime === req.query.datetime) + 1;
      messages = db.getItems().slice(index, db.getItems().length);
      return res.send(messages);
    }
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.author || !req.body.message){
      return res.status(400).send({message: 'Please check that the form is filled out correctly.'})
    } else {
      const message = {
        author: req.body.author,
        message: req.body.message
      }
      await db.addItem(message);
      return res.send(message);
    }
  } catch (e) {
    next(e);
  }
});

module.exports = router;